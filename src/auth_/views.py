from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .serializers import UserAuthOutputSerializer, UserTelegramCreateSerializer


class AuthViewsSet(GenericViewSet):
    serializer_class = UserAuthOutputSerializer

    @swagger_auto_schema(
        request_body=UserTelegramCreateSerializer,
        responses={201: UserAuthOutputSerializer, 400: "errors"},
    )
    @action(["POST"], detail=False)
    def sign_up_telegram(self, request):
        serializer = UserTelegramCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.save()

        return Response(self.get_serializer(user).data, status=status.HTTP_201_CREATED)
