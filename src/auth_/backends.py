from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


class TelegramBackend(ModelBackend):
    def authenticate(self, request, **kwargs):
        telegram_id = kwargs.get("telegram_id")

        if telegram_id is None:
            return

        try:
            user = User._default_manager.get(telegram_id=telegram_id)
        except User.DoesNotExist:
            return
        else:
            if self.user_can_authenticate(user):
                return user
