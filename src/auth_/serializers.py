from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserTelegramCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("telegram_id",)

    def create(self, validated_data):
        validated_data["password"] = ""

        return super(UserTelegramCreateSerializer, self).create(validated_data)


class UserAuthOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "telegram_id",
            "email",
            "first_name",
            "last_name",
        )
