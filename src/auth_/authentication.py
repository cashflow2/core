import base64
import binascii
from abc import ABC

from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header


class TelegramAuthentication(BaseAuthentication, ABC):
    """
    HTTP Basic authentication against telegram_id.
    """

    www_authenticate_realm = "api"

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b"telegram":
            return None

        if len(auth) == 1:
            msg = _("Invalid basic header. No credentials provided.")
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _(
                "Invalid basic header. Credentials string should not contain spaces."
            )
            raise exceptions.AuthenticationFailed(msg)

        try:
            try:
                auth_decoded = base64.b64decode(auth[1]).decode("utf-8")
            except UnicodeDecodeError:
                auth_decoded = base64.b64decode(auth[1]).decode("latin-1")
        except (TypeError, UnicodeDecodeError, binascii.Error):
            msg = _("Invalid basic header. Credentials not correctly base64 encoded.")
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(auth_decoded, request)

    def authenticate_credentials(self, telegram_id, request=None):
        """
        Authenticate the userid and password against username and password
        with optional request for context.
        """
        user = authenticate(request=request, telegram_id=telegram_id)

        if user is None:
            raise exceptions.AuthenticationFailed(_("Invalid telegram id."))

        if not user.is_active:
            raise exceptions.AuthenticationFailed(_("User inactive or deleted."))

        return user, None

    def authenticate_header(self, request):
        return 'Telegram realm="%s"' % self.www_authenticate_realm
