from rest_framework.routers import DefaultRouter

from .views import AuthViewsSet

router = DefaultRouter(trailing_slash=True)
router.register("auth", AuthViewsSet, basename="auth")

urlpatterns = [
    *router.urls,
]
