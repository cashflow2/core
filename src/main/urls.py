from rest_framework.routers import DefaultRouter

from main.views import (
    AccountViewSet,
    CategoryViewSet,
    CreditViewSet,
    DebtViewSet,
    StorageViewSet,
    TransactionViewSet,
    TransferViewSet,
)

router = DefaultRouter(trailing_slash=True)
router.register("storage", StorageViewSet, "storage")
router.register("account", AccountViewSet, "account")
router.register("credit", CreditViewSet, "credit")
router.register("debt", DebtViewSet, "debt")
router.register("transfer", TransferViewSet, "transfer")
router.register(
    r"category/(?P<category_type>expense|income)", CategoryViewSet, "category"
)
router.register(
    r"transaction/(?P<transaction_type>expense|income)",
    TransactionViewSet,
    "transaction",
)

urlpatterns = [
    *router.urls,
]
