from rest_framework import filters


class IsOwnerFilterBackend(filters.BaseFilterBackend):
    default_owner_fields = ("user",)

    def get_owner_fields(self, view):
        return getattr(view, "owner_fields", None) or self.default_owner_fields

    def filter_queryset(self, request, queryset, view):
        owner_fields = self.get_owner_fields(view)
        lookups = {
            f"{owner_field}__exact": request.user for owner_field in owner_fields
        }

        return queryset.filter(**lookups)
