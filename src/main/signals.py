from django.db import transaction
from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver

from main.models import Credit, Debt, Expense, Income, Transfer

# TODO: fire signals only when balance or account are changed


def increase_account_balance(sender, instance, **kwargs):
    instance.account.update_balance(instance.amount)


def decrease_account_balance(sender, instance, **kwargs):
    instance.account.update_balance(-instance.amount)


# pre_save
def edit_account_balance(sender, instance, **kwargs):
    if not instance.pk:
        return

    with transaction.atomic():
        old_record = sender.objects.only("account", "amount").get(pk=instance.pk)
        old_record.account.update_balance(-old_record.amount)

        # after post save is fired a new value is added to account balance


# for transfer
@receiver(post_save, sender=Transfer)
def process_transfer(sender, instance, **kwargs):
    instance.process()


@receiver(pre_delete, sender=Transfer)
def delete_transfer(sender, instance, **kwargs):
    instance.revert()


@receiver(pre_save, sender=Transfer)
def edit_transfer(sender, instance, **kwargs):
    if not instance.pk:
        return

    with transaction.atomic():
        old_instance = sender.objects.only(
            "source", "destination", "amount", "exchange_rate"
        ).get(pk=instance.pk)
        old_instance.revert()

        # after post save is fired new values is added to accounts balances


# creating
post_save.connect(increase_account_balance, Income)
post_save.connect(decrease_account_balance, Expense)
post_save.connect(increase_account_balance, Debt)
post_save.connect(decrease_account_balance, Credit)

# editing
pre_save.connect(edit_account_balance, Income)
pre_save.connect(edit_account_balance, Expense)
pre_save.connect(edit_account_balance, Debt)
pre_save.connect(edit_account_balance, Credit)

# deleting
pre_delete.connect(decrease_account_balance, Income)
pre_delete.connect(decrease_account_balance, Expense)
pre_delete.connect(decrease_account_balance, Debt)
pre_delete.connect(decrease_account_balance, Credit)
