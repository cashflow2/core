from rest_framework import serializers

from main.models import (
    Account,
    Credit,
    Debt,
    Expense,
    ExpenseCategory,
    Storage,
    Transfer,
)


class CurrentUserMixin:
    user_field = "user"

    def create(self, validated_data):
        validated_data[self.user_field] = self.context["request"].user
        return super().create(validated_data)


class StorageSerializer(CurrentUserMixin, serializers.ModelSerializer):
    class Meta:
        model = Storage
        exclude = ("user",)


class AccountSerializer(CurrentUserMixin, serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ("user",)


class CategorySerializer(CurrentUserMixin, serializers.ModelSerializer):
    class Meta:
        # it would be better to use higher model of hierarchy (CategoryBase)
        # but it's abstract so can't be used in a serializer;
        # the purpose of using specific implementation is to auto generate attrs
        model = ExpenseCategory
        exclude = (
            "slug",
            "user",
            "active",
        )


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = "__all__"


class TransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transfer
        fields = "__all__"


class CreditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Credit
        fields = "__all__"


class DebtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Debt
        fields = "__all__"
