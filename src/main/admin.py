from categories.base import CategoryBaseAdmin
from django.contrib import admin

from main.models import (
    Account,
    AccountType,
    Credit,
    Currency,
    Debt,
    Expense,
    ExpenseCategory,
    Income,
    IncomeCategory,
    Storage,
    Transfer,
)

admin.site.register(AccountType)
admin.site.register(Account)
admin.site.register(Credit)
admin.site.register(Currency)
admin.site.register(Debt)
admin.site.register(Expense)
admin.site.register(Income)
admin.site.register(Storage)
admin.site.register(Transfer)
admin.site.register(ExpenseCategory, CategoryBaseAdmin)
admin.site.register(IncomeCategory, CategoryBaseAdmin)
