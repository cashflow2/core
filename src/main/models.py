from categories.base import CategoryBase
from django.contrib.auth import get_user_model
from django.db import models, transaction
from django.db.models import F
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Currency(models.Model):
    symbol = models.CharField(max_length=1, unique=True)
    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name_plural = _("Currencies")

    def __str__(self):
        return "%s" % self.code


class AccountType(models.Model):
    name = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return "%s" % self.name


class Storage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="storages")
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        unique_together = (
            "name",
            "user",
        )


class Account(models.Model):
    is_active = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="accounts")
    storage = models.ForeignKey(
        Storage, on_delete=models.CASCADE, related_name="accounts"
    )
    type = models.ForeignKey(
        AccountType, on_delete=models.CASCADE, related_name="accounts"
    )
    currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="accounts"
    )
    balance = models.DecimalField(max_digits=20, decimal_places=2)
    description = models.CharField(max_length=300, null=True, blank=True)

    def update_balance(self, balance):
        Account.objects.select_for_update().filter(pk=self.pk).update(
            balance=F("balance") + balance
        )


class IncomeCategory(CategoryBase):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="income_categories"
    )

    class Meta:
        verbose_name_plural = _("Income categories")


class ExpenseCategory(CategoryBase):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="expense_categories"
    )

    class Meta:
        verbose_name_plural = _("Expense categories")


class TransactionBase(models.Model):
    date = models.DateField()
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    description = models.CharField(max_length=300, null=True, blank=True)

    class Meta:
        abstract = True


class TransactionBaseWithAccount(TransactionBase):
    account = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name="%(class)s"
    )

    class Meta:
        abstract = True


class Income(TransactionBaseWithAccount):
    category = models.ForeignKey(
        IncomeCategory,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="incomes",
    )


class Expense(TransactionBaseWithAccount):
    category = models.ForeignKey(
        ExpenseCategory,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="expenses",
    )


class Transfer(TransactionBase):
    source = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name="transfers_from"
    )
    destination = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name="transfers_to"
    )
    exchange_rate = models.DecimalField(max_digits=20, decimal_places=2)

    def process(self):
        with transaction.atomic():
            self.source.update_balance(-self.amount)
            self.destination.update_balance(self.amount * self.exchange_rate)

    def revert(self):
        with transaction.atomic():
            self.source.update_balance(self.amount)
            self.destination.update_balance(-(self.amount * self.exchange_rate))


# I borrow money
class Credit(TransactionBaseWithAccount):
    borrower = models.CharField(max_length=100)


# I lend money
class Debt(TransactionBaseWithAccount):
    lender = models.CharField(max_length=100)
