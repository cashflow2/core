from django.apps import apps
from django.db import transaction
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from main.apps import MainConfig
from main.filters import IsOwnerFilterBackend
from main.models import (
    Account,
    Credit,
    Debt,
    Expense,
    ExpenseCategory,
    Storage,
    Transfer,
)
from main.serializers import (
    AccountSerializer,
    CategorySerializer,
    CreditSerializer,
    DebtSerializer,
    StorageSerializer,
    TransactionSerializer,
    TransferSerializer,
)


class AtomicMixin:
    atomic_http_methods = (
        "post",
        "delete",
        "put",
        "patch",
    )

    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() in self.atomic_http_methods:
            with transaction.atomic():
                return super(AtomicMixin, self).dispatch(request, *args, **kwargs)

        return super(AtomicMixin, self).dispatch(request, *args, **kwargs)


class StorageViewSet(ModelViewSet):
    queryset = Storage.objects.all()
    serializer_class = StorageSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]


class AccountViewSet(ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]

    # TODO: add list active accounts as an endpoint or filter


class CategoryViewSet(ModelViewSet):
    # it would be better to use higher model of hierarchy (CategoryBase)
    # but it's abstract so can't be used to get objs by swagger
    model = ExpenseCategory
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        category_type = self.kwargs["category_type"]
        self.model = apps.get_model(
            MainConfig.name, f"{category_type.capitalize()}Category"
        )

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        serializer_class.Meta.model = self.model

        return serializer_class


class TransactionViewSet(AtomicMixin, ModelViewSet):
    model = Expense
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]
    owner_field = "account__user"

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        transaction_type = self.kwargs["transaction_type"]
        self.model = apps.get_model(MainConfig.name, f"{transaction_type.capitalize()}")

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        serializer_class.Meta.model = self.model

        return serializer_class


class TransferViewSet(AtomicMixin, ModelViewSet):
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]
    owner_fields = (
        "source__user",
        "destination__user",
    )


class CreditViewSet(AtomicMixin, ModelViewSet):
    queryset = Credit.objects.all()
    serializer_class = CreditSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]
    owner_fields = ("account__user",)


class DebtViewSet(AtomicMixin, ModelViewSet):
    queryset = Debt.objects.all()
    serializer_class = DebtSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = ModelViewSet.filter_backends + [IsOwnerFilterBackend]
    owner_fields = ("account__user",)
